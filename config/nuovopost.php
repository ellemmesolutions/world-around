<form name="newpost" method="post" action="addpost.php" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <input type="text" name="titolo" placeholder="Titolo" width="100%" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <br />Immagine del blog <br />
                <input type="file" name="userfile" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <textarea name="corpo" placeholder="Il tuo post"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <input type="hidden" name="blog" value="<?php echo $myBlogSel['id']; ?>" width="100%" />
                <input type="submit" value="Crea Post" class="btn btn-primary" />
            </div>
        </div>
    </div>
</form>
