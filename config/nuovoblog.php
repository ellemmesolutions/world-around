<form name="newblog" method="post" action="addblog.php" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <input type="text" name="name" placeholder="Nome del tuo blog" width="100%" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <br />Immagine del blog <br />
                <input type="file" name="userfile" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Crea Blog" class="btn btn-primary" />
            </div>
        </div>
    </div>
</form>
